#!/usr/bin/env ruby

require 'rubygems'
require 'github/markup'

puts GitHub::Markup.render("#{ ARGV[0] }", File.read("#{ ARGV[0] }"))