<?php

/**
 * Implements hook_drush_command().
 */
function apidocs_search_drush_command() {
  $items = array();

  $items['apidocs-clean'] = array(
    'aliases' => array('docclean'),
    'arguments' => array(
      'type' => dt('The type of documents to be scanned.'),
    ),
    'callback' => 'apidocs_search_drush_clean',
    'description' => dt('Removes file references from database that no longer exist in the filesystem.'),
    'examples' => array(
      'drush apidocs-scan' => 'Cleans both github and generated documents',
      'drush apidocs-scan github' => 'Cleans github documents',
      'drush apidocs-scan generated' => 'Cleans generated documents',
    ),
  );

  $items['apidocs-index'] = array(
    'aliases' => array('docindex'),
    'arguments' => array(
      'arg1' => dt('An optional example argument'),
    ),
    'callback' => 'apidocs_search_drush_index',
    'description' => dt('Removes file references from database that no longer exist in the filesystem.'),
    'examples' => array(
      'drush apidocs-index' => 'Indexes known documents in Solr',
    ),
  );

  $items['apidocs-scan'] = array(
    'aliases' => array('docscan'),
    'arguments' => array(
      'type' => dt('The type of documents to be scanned.'),
    ),
    'callback' => 'apidocs_search_drush_scan',
    'description' => dt('Scans existing documentation to record references in the database.'),
    'examples' => array(
      'drush apidocs-scan' => 'Scan both github and generated documents',
      'drush apidocs-scan github' => 'Scan github documents',
      'drush apidocs-scan generated' => 'Scan generated documents',
    ),
  );

  $items['apidocs-markup'] = array(
    'aliases' => array('docmarkup'),
    'arguments' => array(
      'uri' => dt('The uri of the markdown file to markup.'),
    ),
    'callback' => 'apidocs_search_drush_markup',
    'description' => dt('Parses a github flavored markdown file into markup.'),
    'examples' => array(
      'drush apidocs-markup github://fields/doc/Readme.md' => 'Markup the fields/doc/Readme.md file.',
    ),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function apidocs_search_drush_help($command) {
  switch ($command) {
    case 'meta:apidocs_search:title':
      return dt('API docs search');
    case 'meta:apidocs_search:summary':
      return dt('Download, enable, examine and update your modules and themes.');
    case 'drush:apidocs-scan':
      return dt('Scans existing documentation to record references in the database.');
    case 'drush:apidocs-clean':
      return dt('Removes file references from database that no longer exist in the filesystem.');
    case 'drush:apidocs-index':
      return dt('Indexes scanned documentation.');
    case 'drush:apidocs-markup':
      return dt('Parses a github flavored markdown file into markup.');
  }
}

/**
 * Callback function for drush apidocs-clean. 
 *
 * @param $type
 *   An optional string identifying the type of documentation to scan.
 */
function apidocs_search_drush_clean($type = NULL) {
  if (isset($type)) {
    drush_log("Executing apidocs_search_clean($type)", 'ok');
    $output = apidocs_search_clean($type);
  }
  else {
    drush_log("Executing apidocs_search_clean_all()", 'ok');
    $output = apidocs_search_clean_all();
  }
  if (!empty($output['deleted_files'])) {
    drush_log("Deleted Files: " . sizeof($output['deleted_files']), 'ok');
  }
}

/**
 * Callback function for drush apidocs-index. 
 */
function apidocs_search_drush_index() {
  drush_log("Executing apidocs_search_index()", 'ok');
  apidocs_search_index($type);
}

/**
 * Callback function for drush apidocs-scan. 
 *
 * @param $type
 *   An optional string identifying the type of documentation to scan.
 */
function apidocs_search_drush_scan($type = NULL) {
  if (isset($type)) {
    drush_log("Executing apidocs_search_scan($type)", 'ok');
    $output = apidocs_search_scan($type);
  }
  else {
    drush_log("Executing apidocs_search_scan_all()", 'ok');
    $output = apidocs_search_scan_all();
  }
  if (!empty($output['new_files'])) {
    drush_log("New Files: " . sizeof($output['new_files']), 'ok');
  }
  if (!empty($output['updated_files'])) {
    drush_log("Modified Files: " . sizeof($output['updated_files']), 'ok');
  }
}

/**
 * Callback function for drush apidocs-markup. 
 *
 * @param $type
 *   An optional string identifying the type of documentation to scan.
 */
function apidocs_search_drush_markup($uri) {
  drush_log("Executing apidocs_search_markup($uri)", 'ok');
  $output = apidocs_search_markup($uri);
  drush_print_r($output);
}
