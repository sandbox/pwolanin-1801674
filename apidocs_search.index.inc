<?php

/**
 * Remove deleted files from the Solr index.
 */
function apidocs_search_clean_deleted_files($env_id) {
  $ids = array();
  foreach (db_query('SELECT * FROM {apidocs_search_files} asn WHERE asn.status = 0') as $row) {
    $solr_ids[] = apachesolr_document_id($row->entity_id, $row->entity_type);
    $ids[] = $row->entity_id;
  }
  if ($ids) {
    try {
      $solr = apachesolr_get_solr($env_id);
      $solr->deleteByMultipleIds($solr_ids);
      $solr->commit();
      db_delete("apidocs_search_files")
        ->condition("entity_id", $ids)
        ->execute();
      watchdog('apidocs_search', "Deleted @count files from the index", array('@count' => count($ids)));
    }
    catch (Exception $e) {
      watchdog('apidocs_search', "Exception occurred deleting docs in Solr\n!message", array('!message' => nl2br(check_plain($e->getMessage()))), WATCHDOG_ERROR);
    }
  }
}

/**
 * Get a set of file records to index.
 */
function apidocs_search_get_files_to_index($env_id, $limit) {
  extract(apachesolr_get_last_index_position($env_id, 'apidoc.'));
  return db_query_range('SELECT * FROM {apidocs_search_files} asn WHERE (asn.changed > :changed OR (asn.changed = :changed AND asn.entity_id > :last_id)) AND asn.status = 1 ORDER BY asn.changed ASC, asn.entity_id ASC', 0, $limit, array(':changed' => $last_changed, ':last_id' => $last_entity_id))->fetchAll();
}

function apidocs_search_index_execute($env_id, $rows) {
  module_load_include('inc', 'apachesolr', 'apachesolr.index');
  if (empty($rows)) {
    return;
  }
  $documents = array();
  $indexed = 0;
  try {
    foreach ($rows as $row) {
      $documents[] = apidocs_search_solr_document($env_id, $row);
    }
    if ($documents) {
      $indexed = apachesolr_index_send_to_solr($env_id, $documents);
    }
    if ($indexed !== FALSE && !empty($row)) {
      apachesolr_set_last_index_position($env_id, 'apidoc.', $row->changed, $row->entity_id);
    }
  }
  catch (Exception $e) {
    // Suppress - we will try again.
  }
  return $indexed;
}

/**
 * Extract the TITLE or first H1 tag content as the document title.
 */
function apidocs_search_extract_title($string) {
  preg_match('/<title\b[^>]*>(.*?)<\/title>/i', $string, $matches);
  $results = '';
  if (!empty($matches[1])) {
    $results = $matches[1];
  }

  if (empty($results)) {
    preg_match('/<h1\b[^>]*>(.*?)<\/h1>/i', $string, $matches);
    if (!empty($matches[1])) {
      $results = $matches[1];
    }
  }
  return $results;
}

/**
 * Builds the file-specific information for a Solr document.
 *
 * @param array $row
 *   The data from one row in {apidocs_search_files}.
 *
 * @return
 *   An ApacheSolrDocument object.
 *
 * @throws Exception
 */
function apidocs_search_solr_document($env_id, $row) {

  $filedocument = new ApacheSolrDocument();

  $text = apidocs_search_markup($row->uri);

  apachesolr_index_add_tags_to_document($filedocument, $text);

  // Build our separate document and overwrite basic information
  $filedocument->id = apachesolr_document_id($row->entity_id, $row->entity_type);
  $filedocument->url = file_create_url($row->uri);
  $filedocument->site = url(NULL, array('absolute' => TRUE));
  $filedocument->hash = apachesolr_site_hash();

  $filedocument->entity_id = $row->entity_id;
  $filedocument->entity_type = $row->entity_type;
  $filedocument->bundle = $row->bundle;
  $filedocument->bundle_name = $row->bundle;

  // A path is not a requirement of an entity
  if (!empty($filedocument->url)) {
    $filedocument->path = $filedocument->url;
  }

  $filename = basename($row->uri);
  
  // Extract a well formed label.
  $label = apidocs_search_extract_title($text);
  if (empty($label)) {
    $label = $filename;
  }

  // Add extra info to our document
  $filedocument->label = apachesolr_clean_text($label);

  $dirty = variable_get('apidocs_search_dirty_types', array());
  // Some generated docs have dirty HTML that breaks our normal cleanup - try
  // extracting via Tika using SolrCell for more robust cleanup.
  if (in_array($row->bundle, $dirty, TRUE)) {
    // Extract using Solr.
    try {
      list($extracted, $metadata) = apidocs_search_extract_using_solr($env_id, $filename, $text);
      watchdog('apidocs_search', "Extracted @filename of type @bundle using Solr + Tika", array('@filename' => $filename, '@bundle' => $row->bundle), WATCHDOG_NOTICE);
    }
    catch (Exception $e) {
      // Exceptions from Solr may be transient, or indicate a problem with a specific file.
      watchdog('apidocs_search', "Exception occurred sending %filepath to Solr\n!message", array('%filepath' => $row->uri, '!message' => nl2br(check_plain($e->getMessage()))), WATCHDOG_ERROR);
      // Re-throw the exception.
      throw $e;
    }
    $text = $extracted;
  }

  $filedocument->content = preg_replace('/\s\s+/', ' ', apachesolr_clean_text($text));
  $filedocument->teaser = truncate_utf8($filedocument->content, 300, TRUE);

  $filedocument->ds_changed = apachesolr_date_iso($row->changed);

  // Add file specific fields.
  $filedocument->ss_filemime = $row->filemime;
  $filedocument->ss_apisource = $row->entity_type;
  if ($row->entity_type == 'apidoc.github') {
    $filedocument->ss_repopath = file_uri_target($row->uri);
  }

  return $filedocument;
}


/**
 * For given file content, try to extract text using Solr 1.4+.
 *
 * @throws Exception
 */
function apidocs_search_extract_using_solr($env_id, $filename, $text) {
  // Extract using Solr.
  // We allow Solr to throw exceptions - they will be caught
  // by apachesolr.module.
  $solr = apachesolr_get_solr($env_id);
  $params = array(
    'resource.name' => $filename,
    'extractFormat' => 'text', // Matches the -t command for the tika CLI app.
  );
  // Construct a multi-part form-data POST body in $data.
  $boundary = '--' . hash('sha256', uniqid(REQUEST_TIME));
  $data = "--{$boundary}\r\n";
  // The 'filename' used here becomes the property name in the response.
  $data .= 'Content-Disposition: form-data; name="file"; filename="extracted"';
  $data .= "\r\nContent-Type: application/octet-stream\r\n\r\n";
  $data .= $text;
  $data .= "\r\n--{$boundary}--\r\n";
  $headers = array('Content-Type' => 'multipart/form-data; boundary=' . $boundary);
  $options = array(
    'method' => 'POST',
    'headers' => $headers,
    'data' => $data,
  );
  $response = $solr->makeServletRequest(EXTRACTING_SERVLET, $params, $options);
  return array($response->extracted, $response->extracted_metadata);
}
