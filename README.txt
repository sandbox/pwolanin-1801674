
This module looks for docs in 2 directories one
level above your docroot:

1) generated
2) github

Each is handled by a custom stream wrapper.

Docs in generated will be served by the Drupal site to users
who are logged in and have permission.

.md, .txt, and .html docs in each directory can be
scanned and queued for indexing to Solr. The .md
files will be rendered into github-flavored markup
using the github ruby tool. You need to have the
github/markup gem installed.

You need a Solr instance set up for tika extraction -
see the apachesolr_attachements module for details on that.

The job.sh file is an example script for populating docs
in a repo via Jenkins or another automated build process.
The Jenkins job can use the git plugin to checkout
and push to the "allthethings" repo, and the job
woudld look very short like:

export ALLTHINGS=$WORKSPACE/allthethings
bash -ex $ALLTHINGS/docroot/sites/all/modules/apidocs_search/job.sh \
  -r "foo" -r "bar" \
  -g "foo/rstuff"

To copy docs from the foo and bar repos, and generate RDoc from
the foo/rstuff code.
