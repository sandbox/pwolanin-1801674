<?php


/**
 * Drupal generated:// stream wrapper class.
 *
 * Provides support for storing publicly accessible files with the Drupal file
 * interface.
 */
class apidocsGeneratedStreamWrapper extends DrupalLocalStreamWrapper {
  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return realpath(DRUPAL_ROOT. '/../generated');
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('apidocs-search/view/generated/' . $path, array('absolute' => TRUE));
  }
}


/**
 * Drupal github:// stream wrapper class.
 *
 * Provides support for storing privately accessible files with the Drupal file
 * interface.
 *
 * Extends DrupalPublicStreamWrapper.
 */
class apidocsGithubStreamWrapper extends DrupalLocalStreamWrapper {
  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return realpath(DRUPAL_ROOT. '/../github');
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a private file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    $path_components = explode("/", $path);
    // Extract the repo
    $repo = $path_components[0];
    unset($path_components[0]);
    $path_in_repo = implode("/",$path_components);

    return "https://github.com/acquia/$repo/blob/master/$path_in_repo";
  }
}
