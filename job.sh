#!/bin/bash

# $ALLTHINGS tells use where the repo is

if [ -z "$ALLTHINGS" ]; then
  echo "Please define env variable ALLTHINGS"
  exit 1
fi

if [ -z "$@" ]; then
  echo "Usage:  $0 -r REPONAME -g REPONAME/GEMDIR"
  exit 2
fi

while getopts ":g:r:" optname
  do
    case "$optname" in
      "g")
        gems[$OPTIND]=$OPTARG
        ;;
      "r")
        repos[$OPTIND]=$OPTARG
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 3
        ;;
      ":")
        echo "No argument value for $OPTARG"
        exit 4
        ;;
      *)
      # Should not occur
        echo "Error while processing options"
        exit 5
        ;;
    esac
  done

echo "repos: ${repos[@]}"

echo "gems to build RDoc: ${gems[@]}"

# Standardize on <repo>/doc ... copy existing markdown
for i in ${repos[@]};
do
  mkdir -p $ALLTHINGS/github/$i/
  if [ ! -d $WORKSPACE/$i/.git ]; then
    rm -rf $WORKSPACE/$i
    git clone git@github.com:acquia/$i.git $WORKSPACE/$i
  fi
  cd $WORKSPACE/$i
  git fetch --all
  git reset --hard origin
  for dir in $WORKSPACE/$i/{doc,docs}; do
    [ ! -d $dir ] || cp -R $dir $ALLTHINGS/github/$i/
  done
  # Copy docs at the top level of the repo too.
  for file in $WORKSPACE/$i/*.{md,txt,htm,html}; do
    [ ! -f $file ] || cp $file $ALLTHINGS/github/$i/
  done
done

# Generate rdocs for api's
for i in ${gems[@]};
do
  cd $WORKSPACE/$i
  name=$(basename $i)
  rdoc -o $ALLTHINGS/generated/$name
done

cd $ALLTHINGS

# Only commit all the things if there are changes
git add generated
git add github
if ! git diff-index --quiet HEAD --; then
  git status
  git commit -am "Automated document generation: ${JOB_NAME} build ${BUILD_NUMBER}"
fi
